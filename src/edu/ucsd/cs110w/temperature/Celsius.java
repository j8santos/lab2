/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author j8santos
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: Complete this method
		return "" + this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		Temperature temp = new Celsius(this.getValue());
		
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		Temperature temp = new Fahrenheit((float) (this.getValue()*1.8)+32);
		return temp;
	}
}
