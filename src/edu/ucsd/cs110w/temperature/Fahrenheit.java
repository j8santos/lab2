/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author j8santos
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: Complete this method
		return "" + this.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		Temperature temp = new Celsius((this.getValue()-32)*((float) 5/9));
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		Temperature temp = new Fahrenheit(this.getValue());
		return temp;
	}
}
